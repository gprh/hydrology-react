// src/App.js

import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from './pages/LoginPage/LoginPage';
import SignUpPage from './pages/SignUpPage/SignUpPage';
import PrivateRoute from './components/PrivateRoutes';
import CadastroProfessores from './components/CadastroProfessores';
import CoordenacaoHome from './components/CoordenacaoHome/CoordenacaoHome';
import AlunoHome from './pages/AlunoHome/AlunoHome';
import CadastroContas from './pages/CadastroContas';
import Home from './pages/Home/Home';
import CadastroProjetos from './pages/CadProjetos/CadProjetos';
import { UserProvider } from './context/UserContext';


function App() {
  return (
    <UserProvider>
      <Router>

        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/signup" element={<SignUpPage />} />
          <Route
            path="/cadastrar-prof"
            element={
              <PrivateRoute>
                <CadastroProfessores />
              </PrivateRoute>
            }
          />
          <Route
            path="/coordenacao-home"
            element={
              <PrivateRoute>
                <CoordenacaoHome />
              </PrivateRoute>
            }
          />
          <Route
            path="/home"
            element={
              <PrivateRoute>
                <Home />
              </PrivateRoute>
            }
          />
          <Route
            path="/aluno-home"
            element={
              <PrivateRoute>
                <AlunoHome />
              </PrivateRoute>
            }
          />

          <Route
            path="/cadastrar-conta"
            element={
              <PrivateRoute>
                <CadastroContas />
              </PrivateRoute>
            }
          />

          <Route
            path="/cadastrar-projetos"
            element={
              <PrivateRoute>
                <CadastroProjetos />
              </PrivateRoute>
            }
          />


        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
