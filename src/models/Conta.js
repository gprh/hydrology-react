// src/models/Conta.js

class Conta {
    constructor(idPai = '', nome = '', indice='', isCustoFixo = '', isReceita = 0) {        
        this.idPai = idPai;
        this.nome = nome;
        this.indice = indice;
        this.isCustoFixo = isCustoFixo;
        this.isReceita = isReceita;        
    }
}

export default Conta;
