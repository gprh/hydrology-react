import React, { useState, useEffect } from 'react';
import Modal from "react-modal";
import './LancamentoHome.css';
import FormLancamento from '../../components/Lancamentos/FormLancamento/FormLancamento';
import {
  getContas
} from "../../api/contaApi";
import {
  getLancamentos,
  updateLancamento,
  deleteLancamento,
  createLancamento
} from "../../api/lancamentosApi";
import TableLancamentos from '../../components/Lancamentos/TableLancamento/TableLancamentos';
import Diario from "../../models/Diario";
import EstacoesAbertas from "../../components/EstacoesAbertas";

Modal.setAppElement("#root");

function LancamentoHome() {
  const [contas, setContas] = useState([]);

  const [lancamentos, setLancamentos] = useState([]);

  const [editingDiario, setEditingDiario] = useState(new Diario());
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const [filteredLancamentos, setFilteredLancamentos] = useState(lancamentos);

  //Acesso ao banco para buscar as contas
  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getContas();
        setContas(data);
        console.log("Acesso ao firebase.");
      } catch (error) {
        console.error("Erro ao buscar contas:", error);
      }
    };

    fetchData();
  }, []);


  //Acesso ao banco para buscar os lancamentos
  useEffect(() => {
    const buscarLancamentos = async () => {
      try {
        const data = await getLancamentos(); // Sua função de API para buscar os lançamentos
        setLancamentos(data);
      } catch (error) {
        console.error('Erro ao buscar lançamentos:', error);
        // Aqui você pode definir um estado de erro ou mostrar um feedback ao usuário
      }
    };

    buscarLancamentos();
  }, []);

  const findChildAccountIds = (accountId, accounts) => {
    const childIds = [];
    const queue = [accountId];

    while (queue.length > 0) {
      const currentId = queue.shift();
      childIds.push(currentId);

      accounts.forEach(account => {
        if (account.idPai === currentId) {
          queue.push(account.id);
        }
      });
    }

    return childIds;
  };

  const filterLancamentosByAccountId = (accountId) => {
    const childAccountIds = findChildAccountIds(accountId, contas);
    const filtered = lancamentos.filter(lancamento =>
      childAccountIds.includes(lancamento.idConta)
    );

    // Calcula a soma dos valores dos lançamentos filtrados
    const totalValor = filtered.reduce((sum, lancamento) => sum + lancamento.valor, 0);

    setFilteredLancamentos(filtered);

    // Aqui você pode escolher o que fazer com o totalValor. Por exemplo:
    // Exibir na tela, guardar em um estado, etc.
    console.log(`Total Valor: R$ ${totalValor.toFixed(2)}`);

  };

  const handleContaSelect = (accountId) => {
    console.log("o id da conta clicado foi:", accountId);
    filterLancamentosByAccountId(accountId);
  };


  const handleAddOrUpdate = async (diario) => {
    try {
      console.log("handleAddOrupdate:", editingDiario, diario);
      if (editingDiario && editingDiario.id) {
        const updatedDiario = await updateLancamento(editingDiario.id, diario);
        setLancamentos(
          lancamentos.map((diario) =>
            diario.id === editingDiario.id ? updatedDiario : diario
          )
        );
        setEditingDiario(null);
      } else {
        console.log("else:", diario);
        const newDiario = await createLancamento(diario);
        if (newDiario) {
          setLancamentos([...lancamentos, newDiario]);
        }
      }

      setIsModalOpen(false);
      setErrorMessage(null); // Clear error if operation is successful
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  const handleAdd = () => {
    const today = new Date();
    const formattedDate = today.toISOString().substring(0, 10);
    let diarioAux = new Diario();
    diarioAux.data = formattedDate;
    console.log(diarioAux.data);
    setEditingDiario(diarioAux);
    // console.log("handleAdd");
    // setEditingDiario(null);
    setIsModalOpen(true);
  };

  const handleEdit = (diario) => {
    setEditingDiario(diario);
    setIsModalOpen(true);
  };

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm(
      "Você tem certeza de que deseja excluir este lancamento?"
    );
    if (confirmDelete) {
      if (await deleteLancamento(id))
        setLancamentos(lancamentos.filter((d) => d.id !== id));
      else
        alert("Operação inválida.");
    }
  };

  return (
    <div className="lancamento-home">
      <h1 className="title">Base Hidrológica</h1>
      <div className="lancamento-container">
        <div className="sidebar">
          <EstacoesAbertas />
        </div>
        <div className="content">
          <button className="add-btn" onClick={handleAdd}>
            Adicionar
          </button>
          <Modal
            isOpen={isModalOpen}
            onRequestClose={() => setIsModalOpen(false)}
            contentLabel="Adicionar"
          >
            <FormLancamento
              contas={contas}
              lancamentoSelecionado={editingDiario}
              onSubmit={handleAddOrUpdate}
            />
            <button className="close-btn" onClick={() => setIsModalOpen(false)}>
              Fechar
            </button>
          </Modal>

          <TableLancamentos
            lancamentos={filteredLancamentos}//{lancamentos}
            onEdit={handleEdit}
            onDelete={handleDelete}
          />
        </div>
      </div>
    </div>
  );
}

export default LancamentoHome;
