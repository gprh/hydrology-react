// src/components/ProjetoForm.js
import React, { useState } from "react";
import Projeto from "../../model/Projeto";

const ProjetoForm = ({ onSubmit, projeto }) => {
  const [projData, setProjData] = useState(projeto || new Projeto());

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(projData);
    setProjData(new Projeto()); // Reset the form
  };

  return (
    <form onSubmit={handleSubmit} className="projeto-form">
      <label>
        Nome do Projeto:
        <input
          type="text"
          value={projData.nome}
          onChange={(e) => setProjData({ ...projData, nome: e.target.value })}
          required
        />
      </label>
      <label>
        Tipo:
        <select
          value={projData.tipo}
          onChange={(e) => setProjData({ ...projData, tipo: e.target.value })}
          required
        >
          <option value="streamflow">Streamflow</option>
          <option value="rainflow">Rainflow</option>
        </select>
      </label>
      <label>
        Data:
        <input
          type="date"
          value={projData.data}
          onChange={(e) => setProjData({ ...projData, data: e.target.value })}
          required
        />
      </label>
      <label>
        Somente Dados Consistidos:
        <input
          type="checkbox"
          checked={projData.somenteConsistidos}
          onChange={(e) => setProjData({ ...projData, somenteConsistidos: e.target.checked })}
        />
      </label>
      <button type="submit">{projeto ? "Atualizar" : "Adicionar"}</button>
    </form>
  );
};

export default ProjetoForm;
