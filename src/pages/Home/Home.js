import React, { useState } from 'react';
import './Home.css';
import CadastroProfessores from '../../components/CadastroProfessores/CadastroProfessores';
import AlunoHome from '../AlunoHome/AlunoHome';
import LancamentoHome from '../LancamentoHome/LancamentoHome';
import CadastroContas from '../CadastroContas';
import ContaTreeView from '../../components/Conta/ContaTreeView/ContaTreeView'; // Importe seu componente ContaTreeView
import CadastroProjetos from '../CadProjetos/CadProjetos';

function Home() {
  const [selectedContent, setSelectedContent] = useState(null);

  return (
    <div className="home">
      <div className="top-menu">
        <ul>
         <li onClick={() => setSelectedContent('projetos')}>Projetos</li>
          <li onClick={() => setSelectedContent('lancamento')}>Lancamentos</li>
          <li onClick={() => setSelectedContent('contas')}>Cadastro de Contas</li>
          <li onClick={() => setSelectedContent('professor')}>Cadastro de Professor</li>
          <li onClick={() => setSelectedContent('aluno')}>Cadastro de Aluno</li>
        </ul>
      </div>
      <div className="content">
        {selectedContent === 'projetos' && <CadastroProjetos/>}
        {selectedContent === 'contas' && <CadastroContas />}
        {selectedContent === 'lancamento' && <LancamentoHome />}
        {selectedContent === 'professor' && <CadastroProfessores />}
        {selectedContent === 'aluno' && <AlunoHome />}
      </div>
    </div>
  );
}

export default Home;
