import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import ContaForm from "../../components/Conta/ContaForm";
import {
  getContas,
  createConta,
  updateConta,
  deleteConta
} from "../../api/contaApi";
import "./CadastroContas.css";
import ContaTreeView from "../../components/Conta/ContaTreeView/ContaTreeView";
import Conta from "../../models/Conta";

Modal.setAppElement("#root");

const CadastroContas = () => {
  const [contasTree, setContasTree] = useState([]);
  const [contas, setContas] = useState([]);
  const [editingConta, setEditingConta] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getContas();
        setContas(data);
        console.log("Acesso ao firebase.");
      } catch (error) {
        console.error("Erro ao buscar contas:", error);
        setErrorMessage("Erro ao carregar contas.");
      }
    };
  
    fetchData();
  }, []);
  
  useEffect(() => {
    const treeData = buildTree(contas);
    setContasTree(treeData);   
  }, [contas]); // Depende de `contas` para reconstruir a árvore quando `contas` é atualizado


  const buildTree = (contas, parentId = null) => {
    return contas.filter(conta => conta.idPai === parentId)
                 .map(conta => {
                   return {
                     ...conta,
                     children: buildTree(contas, conta.id)
                   };
                 });
  };
  
  const handleAddOrUpdate = async (conta) => {
    try {      
      if(conta.children)
        delete conta.children;

      if (editingConta.id) {
        const updatedConta = await updateConta(editingConta.id, conta);
        setContas(
          contas.map((c) =>
            c.id === editingConta.id ? updatedConta : c
          )
        );
        setEditingConta(null);
      } else {
        const newConta = await createConta(conta);
        if (newConta) {
          setContas([...contas, newConta]);
        }
      }
     
      setIsModalOpen(false);
      setErrorMessage(null); // Clear error if operation is successful
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm(
      "Você tem certeza de que deseja excluir esta conta?"
    );
    if (confirmDelete) {
      if(await deleteConta(id))
        setContas(contas.filter((c) => c.id !== id));
      else
        alert("Operação inválida. Esta conta possui filhos.");
    }
  };

  const handleAdd = (conta) => {
    //limpo a conta e seto apenas o pai, afinal vai ser chmado no clique na propria conta pai.
    let newConta = new Conta();
    newConta.idPai = conta.id;
    setEditingConta(newConta);
    setIsModalOpen(true);
  };

  const handleEdit = (conta) => {
    setEditingConta(conta);
    setIsModalOpen(true);
  };

  return (
    <div className="cadastro-contas">
      <h1 className="title">Cadastro de Contas</h1>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        contentLabel="Adicionar Conta"
      >
        <div className="modal-content">
          <ContaForm
            onSubmit={handleAddOrUpdate}
            contaSelecionada={editingConta}
            contas={contas}
          />
          <button className="close-btn" onClick={() => setIsModalOpen(false)}>
            Fechar
          </button>
        </div>
      </Modal>

      <ContaTreeView
        contas={contasTree}
        onEdit={handleEdit}        
        onAdd={handleAdd}
        onDelete={handleDelete}
      />
      {errorMessage && <p className="error-message">{errorMessage}</p>}
    </div>
  );
};

export default CadastroContas;
