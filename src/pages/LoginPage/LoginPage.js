import React, { useState } from "react";
import { auth } from "../../firebaseConfig";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
} from "firebase/auth";
import Input from "../../components/Input";
import { MdEmail, MdLock, MdPersonAdd } from "react-icons/md";
import "./LoginPage.css";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import ProjetoSelectDialog from "../CadProjetos/components/ProjetoSelectDialog/ProjetoSelectDialog";
import { useUser } from "../../context/UserContext";

import hydrologyPlusImage from '../../assets/HydrologyPlus.png'; // Certifique-se que o caminho está correto


function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const { user, setUser, currentProject, setCurrentProject } = useUser();

  const navigate = useNavigate();

  const handleLoginSuccess = (user) => {
    setUser(user);
    navigate("/cadastrar-projetos"); // Substitua por sua rota correta
  };


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const userCredential = await signInWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;
      handleLoginSuccess(userCredential.user);

    } catch (err) {
      setError(err.message);
    }
  };

  const handleSignUp = async () => {
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      // Redirecione para a página inicial ou outra página após o cadastro bem-sucedido
      handleLoginSuccess();
    } catch (err) {
      setError(err.message);
    }
  };



  const handleForgotPassword = async () => {
    try {
      await sendPasswordResetEmail(auth, email);
      alert("Verifique seu e-mail para redefinir a senha.");
    } catch (err) {
      setError(err.message);
    }
  };


  return (

    <div className="login-home">
      <div className="login-container">
        <div className="brand-section">
          <img src={hydrologyPlusImage} alt="Hydrology Plus" className="brand-image" />
        </div>
        <div className="login-cadastro">
          <form className="form-section" onSubmit={handleSubmit}>
            <Input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              icon={<MdEmail size={20} color="#007bff" />}
              placeholder="Email"
            />
            <Input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              icon={<MdLock size={20} color="#007bff" />}
              placeholder="Password"
            />
            <button className="login-button" type="submit">Login</button>
          </form>
          {error && <p className="error-message">{error}</p>}

          <div className="signup-forgot">
            <Link to="/signup">
              <button className="signup-button" onClick={handleSignUp}>
                <MdPersonAdd size={20} color="#007bff" />
                Criar Conta
              </button>
            </Link>
            <button className="forgot-button" onClick={handleForgotPassword}>
              Esqueci a Senha
            </button>
          </div>
        </div>
       
      </div>
    </div>
  );
}

export default LoginPage;
