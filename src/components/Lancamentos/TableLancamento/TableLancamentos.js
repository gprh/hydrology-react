import React, { useState, useEffect } from 'react';
import './TableLancamentos.css'; // Certifique-se de criar um arquivo de estilo CSS apropriado
import { FaEdit, FaTrash } from 'react-icons/fa'; // Importando ícones

import {
    getConta // Assumindo que esta função existe e pode ser usada para buscar uma conta pelo ID
} from "../../../api/contaApi";
import { format, utcToZonedTime } from 'date-fns-tz';

const TableLancamentos = ({ lancamentos, onEdit, onDelete }) => {
    // Estado para armazenar os nomes das contas referenciadas nos lançamentos
    const [nomesContas, setNomesContas] = useState({});

    // Função para buscar o nome da conta e atualizar o estado
    const fetchNomesContas = async (idConta) => {
        if (idConta && !nomesContas[idConta]) {
            try {
                const conta = await getConta(idConta);
                setNomesContas(prevNomes => ({ ...prevNomes, [idConta]: conta.nome }));
            } catch (error) {
                console.error('Erro ao buscar nome da conta:', error);
                setNomesContas(prevNomes => ({ ...prevNomes, [idConta]: 'Erro ao carregar' }));
            }
        }
    };

    // Efeito para buscar os nomes das contas quando o componente é montado ou os lançamentos são atualizados
    useEffect(() => {
        lancamentos.forEach(({ idConta }) => fetchNomesContas(idConta));
    }, [lancamentos]);

    if (!lancamentos || lancamentos.length === 0) {
        return <p>Nenhum lançamento encontrado.</p>;
    }

    return (
        <table className="table-lancamentos">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Conta</th>
                    <th>Valor</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                {lancamentos.map((diario) => {
                    // Converter a data para o fuso horário local antes de formatar
                    const zonedDate = utcToZonedTime(new Date(diario.data + 'T00:00:00'), Intl.DateTimeFormat().resolvedOptions().timeZone);
                    return (
                        <tr key={diario.id}>
                            <td>{format(zonedDate, 'dd/MM/yyyy')}</td>
                            <td>{nomesContas[diario.idConta] || 'Carregando...'}</td>
                            <td>{`R$ ${diario.valor.toFixed(2)}`}</td>
                            <td>{diario.descricao}</td>
                            <td>
                                <FaEdit className="icon-edit" onClick={() => onEdit(diario)} />
                                <FaTrash className="icon-remove" onClick={() => onDelete(diario.id)} />
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default TableLancamentos;
