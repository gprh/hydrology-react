import React, { useState } from 'react';
import "./FormLancamento.css";
import Select from 'react-select';
import Diario from '../../../models/Diario';

const FormLancamento = ({ onSubmit, lancamentoSelecionado, contas }) => {
    const [diario, setDiario] = useState(lancamentoSelecionado || new Diario());

    const handleSave = (e) => {
        // Aqui você inseriria a lógica para enviar os dados ao servidor ou a outro serviço
        e.preventDefault();
        console.log("FormLancamento, antes de salvar:", diario);
        onSubmit(diario);
        setDiario(new Diario()); // Reset formulario
    };


    const handleCancel = () => {
        // Lógica para cancelar o preenchimento do formulário
        setDiario(new Diario());
    };

    const handleSelect = (selectedOption) => {
        setDiario({ ...diario, idConta: selectedOption ? selectedOption.value : '' });
    };

    const getSelectedAccountValue = () => {
        if (!diario.idConta) {
            return null; // Se não houver conta selecionada, retorna nulo para mostrar o placeholder.
        }

        // Encontrar a conta no array de contas que corresponde ao idConta de diario.
        const selectedAccount = contas.find(conta => conta.id === diario.idConta);

        if (selectedAccount) {
            return { value: selectedAccount.id, label: selectedAccount.nome }; // Retorna o objeto esperado pelo Select.
        } else {
            return null; // Retorna nulo se a conta com o idConta não for encontrada.
        }
    };


    return (
        <div className="FormLancamento">
            <h1>Lançamento</h1>
            <form>
                <label>
                    Conta
                    <Select
                        value={getSelectedAccountValue()} //é usado para mostrar ao usuario a mudanca de estado.
                        onChange={handleSelect} // é feito antes do value.
                        options={contas.map(conta => ({ value: conta.id, label: conta.nome }))}
                        placeholder="Selecione a conta"
                    />

                </label>
                <input
                    type="number"
                    value={diario.valor === 0 ? '' : diario.valor}
                    onChange={(e) => setDiario({ ...diario, valor: e.target.value ? parseFloat(e.target.value) : 0 })}
                    required
                />

                <label>
                    Data
                    <input
                        type="date"
                        value={diario.data}
                        onChange={(e) => setDiario({ ...diario, data: e.target.value })}
                    />
                </label>
                <label>
                    Descrição
                    <textarea
                        value={diario.descricao}
                        onChange={(e) => setDiario({ ...diario, descricao: e.target.value })}
                    />
                </label>
                <div className="button-group">
                    <button type="button" onClick={handleSave}>Salvar</button>
                    <button type="button" onClick={handleCancel}>Cancelar</button>
                </div>
            </form>
        </div>
    );
};

export default FormLancamento;
