// src/components/EstacaoTable.js

import React from 'react';
import './EstacaoTable.css';
import { FaEdit, FaTrash, FaEye } from 'react-icons/fa'; // Importando ícones

const EstacaoTable = ({ estacoes, onEdit, onDelete, onSelect }) => {
    if (!estacoes || estacoes.length === 0) {
        return <p>Nenhum estacao encontrado.</p>;
    }

    return (
        <table className="estacao-table">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Inicio</th>
                    <th>Fim</th>
                    <th>Área</th>
                    <th>Ações</th>
                    <th>Abrir</th>
                </tr>
            </thead>
            <tbody>
                {estacoes.map(estacao => (
                    <tr key={estacao.id}>
                        <td>{estacao.codigo}</td>     
                        <td>{estacao.inicio}</td>
                        <td>{estacao.fim}</td>
                        <td>{estacao.area}</td>                             
                        <td>
                            <FaEdit className="icon-edit" onClick={() => onEdit(estacao)} />
                            <FaTrash className="icon-remove" onClick={() => onDelete(estacao.id)} />
                        </td>
                        <td>
                            <FaEye className="icon-remove" onClick={() => onSelect(estacao)} />
                        </td>

                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default EstacaoTable;
