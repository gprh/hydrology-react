import React, { useState } from "react";
import Estacao from "../../model/Estacao";
import { FaFolderOpen } from 'react-icons/fa'; // Importando ícones


const EstacaoForm = ({ onSubmit, estacao }) => {
  const [estData, setEstData] = useState(estacao || new Estacao());

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      // Aqui você pode processar o arquivo ou apenas armazenar o caminho
      setEstData({ ...estData, textoCsv: file.name }); // Usando o nome do arquivo por enquanto
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(estData);
    setEstData(new Estacao()); // Reset the form
  };

  return (
    <form onSubmit={handleSubmit} className="estacao-form">
      <label>
        Caminho do arquivo CSV:
        <input
          type="text"
          value={estData.textoCsv}
          onChange={(e) => setEstData({ ...estData, textoCsv: e.target.value })}
          required
        />
        <button type="button" onClick={() => document.getElementById('fileInput').click()}>
          <FaFolderOpen  />
        
        </button>
        <input
          type="file"
          id="fileInput"
          style={{ display: 'none' }}
          onChange={handleFileChange}
          accept=".csv"
        />
      </label>
      <label>
        Área:
        <input
          type="text"
          value={estData.area}
          onChange={(e) => setEstData({ ...estData, area: e.target.value })}
          required
        />
      </label>
      <button type="submit">{estacao ? "Atualizar" : "Adicionar"}</button>
    </form>
  );
};

export default EstacaoForm;
