import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import EstacaoTable from "./components/EstacaoTable";
import EstacaoForm from "./components/EstacaoForm";
import {
  getEstacoes,
  createEstacao,
  updateEstacao,
  deleteEstacao
} from "../../api/estacoesApi";
import "./EstacoesAbertas.css";
import { useUser } from "../../context/UserContext";

Modal.setAppElement("#root");

function EstacoesAbertas() {
  const [estacoes, setEstacoes] = useState([]);
  const [editingEstacoes, setEditingEstacoes] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const { currentProject, setCurrentProject } = useUser();

  useEffect(() => {
    const fetchData = async () => {
      const data = await getEstacoes();
      setEstacoes(data);
    };

    fetchData();
  }, []);

  const handleAddOrUpdate = async (estacao) => {
    try {
      if (editingEstacoes) {
        const updateEstacao1 = await updateEstacao(editingEstacoes.id, estacao);
        setEstacoes(
          estacoes.map((p) => p.id === editingEstacoes.id ? updateEstacao1 : p)
        );
        setEditingEstacoes(null);
      } else {
        const newEstacao = await createEstacao(estacao);
        if (newEstacao) {
          setEstacoes([...estacoes, newEstacao]);
        }
      }
      setIsModalOpen(false);
      setErrorMessage(null); // Clear error if operation was successful
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm(
      "Você tem certeza de que deseja excluir esta estacao do projeto?"
    );
    if (confirmDelete) {
      await deleteEstacao(id);
      setEstacoes(estacoes.filter((p) => p.id !== id));
    }
  };

  const handleEdit = (estacao) => {
    setEditingEstacoes(estacao);
    setIsModalOpen(true);
  };

  const handleSelectEstacao = (estacao) => {


  };

  return (
    <div className="estacoes-abertas">
      <h1 className="title"> Estações abertas </h1>

      <button className="add-btn" onClick={() => setIsModalOpen(true)}>
        Add estação
      </button>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        contentLabel="Adicionar Estação"
      >
        <div className="modal-content">
          <EstacaoForm
            onSubmit={handleAddOrUpdate}
            projeto={editingEstacoes} />
          <button className="close-btn" onClick={() => setIsModalOpen(false)}>
            Fechar
          </button>
        </div>
      </Modal>

      <EstacaoTable
        estacoes={estacoes}
        onEdit={handleEdit}
        onDelete={handleDelete}
        onSelect={handleSelectEstacao} />
    </div>
  );
}

export default EstacoesAbertas;
