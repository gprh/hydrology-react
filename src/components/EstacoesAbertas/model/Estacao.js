class Estacao {
    constructor(codigo='', nome = '', tipo = 'streamflow', inicio = new Date().toISOString().substring(0, 10), fim = new Date().toISOString().substring(0, 10),
    disponibilidade='', qmld=0, textoCsv='', sintetica = false) {        
        this.codigo = codigo;
        this.nome = nome;
        this.tipo = tipo;
        this.inicio = inicio;
        this.fim = fim;
        this.disponibilidade = disponibilidade;
        this.qmld = qmld;
        this.textoCsv = textoCsv;
        this.sintetica = sintetica;
    }
}

export default Estacao;

