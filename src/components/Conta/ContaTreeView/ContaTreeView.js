import React, { useState, useEffect } from 'react';
import ContaTreeItem from './ContaTreeItem';
import './ContaTreeView.css';

const ContaTreeView = ({ lstContas, onEdit, onDelete, onAdd, onContaSelect }) => {
    const [contas, setContas] = useState([]);
    const [contasTree, setContasTree] = useState([]);

    useEffect(() => {
        if (lstContas) {
            console.log("contasLst", lstContas);
            setContas(lstContas);
        }
    }, [lstContas]);

    useEffect(() => {
        if (contas.length) {
            const treeData = buildTree(contas);
            //console.log("contasTree", treeData);
            setContasTree(treeData);
        }
    }, [contas]); // Depende de `contas` para reconstruir a árvore quando `contas` é atualizado


    const buildTree = (contas, parentId = null) => {
        return contas.filter(conta => conta.idPai === parentId)
            .map(conta => {
                return {
                    ...conta,
                    children: buildTree(contas, conta.id)
                };
            });
    };


    const renderChildren = (children) => {
        if (children) {
            return renderTree(children);
        } else {
            return null;
        }
    };

    const renderTree = (contasTree) => {
        return contasTree.map(conta => (
            <ContaTreeItem
                key={conta.id}
                conta={conta}
                onContaSelect={onContaSelect} 
                onEdit={onEdit}
                onDelete={onDelete}
                onAdd={onAdd}
                children={renderChildren(conta.children)}
            />
        ));
    };

    return (
        <div>
            <div className="conta-tree-view">
                {renderTree(contasTree)}
            </div>
        </div>
    );
};

export default ContaTreeView;
