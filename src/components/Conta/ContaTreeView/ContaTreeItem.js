import React, { useState, useRef, useEffect } from 'react';
import { FaChevronRight, FaChevronDown, FaEllipsisH, FaEdit, FaTrashAlt } from 'react-icons/fa';
import { VscNewFile } from 'react-icons/vsc';
import './ContaTreeItem.css';

const ContaTreeItem = ({ conta, children, onEdit, onDelete, onAdd, onContaSelect}) => {
    const [isExpanded, setIsExpanded] = useState(false);
    const [showMenu, setShowMenu] = useState(false);

    //para controlar o saldo e ...
    const [showSaldo, setShowSaldo] = useState(false);
    const [showIcons, setShowIcons] = useState(false);

    const menuRef = useRef(); // Ref para o menu dropdown para detectar cliques fora

    const handleToggle = (event) => {
        event.stopPropagation();
        setIsExpanded(!isExpanded);
    };

    const handleMenuClick = (event) => {
        event.stopPropagation();
        setShowMenu(!showMenu);
    };

    // Função para obter saldo formatado
    const getFormattedSaldo = () => {
        // Se conta.saldo for um número, formate-o, senão retorne '0.00'
        return typeof conta.saldo === 'number' ? 'R$' + conta.saldo.toFixed(2) : '';
    };

    // Fecha o menu se clicar fora
    useEffect(() => {
        const handleClickOutside = (event) => {
            if (menuRef.current && !menuRef.current.contains(event.target)) {
                setShowMenu(false);
            }
        };

        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [menuRef]);

    // Menu dropdown
    const renderMenu = () => (
        <div ref={menuRef} className="menu-dropdown">
            <div className="menu-item" onClick={() => onEdit(conta)}>
                <FaEdit /> Editar
            </div>
            <div className="menu-item" onClick={() => onDelete(conta.id)}>
                <FaTrashAlt /> Excluir
            </div>
        </div>
    );

    const handleContaClick = () => {
        onContaSelect(conta.id); // Notifica o componente pai que uma conta foi selecionada
      };

    return (
        <div>
            <div className="conta-tree-item" onClick={handleContaClick}>
                {children && children.length > 0 && (
                    <span onClick={handleToggle} style={{ cursor: 'pointer', marginRight: '8px' }}>
                        {isExpanded ? <FaChevronDown /> : <FaChevronRight />}
                    </span>
                )}

                <span>
                    {conta.nome}
                </span>

                <div className="saldo-field">
                    {getFormattedSaldo()}
                </div>

                <div className="icon-container">
                    <span className="icon-tree-dots" onClick={handleMenuClick}>
                        <FaEllipsisH />
                    </span>

                    <span className="icon-tree-addFilha" onClick={() => onAdd(conta)}>
                        <VscNewFile />
                    </span>
                </div>
                {showMenu && renderMenu()}
            </div>
            {isExpanded && (
                <div className="conta-tree-children">
                    {children}
                </div>
            )}
        </div>
    );
};

export default ContaTreeItem;
