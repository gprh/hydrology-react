import React, { useState, useEffect } from 'react';
import Tree from 'react-animated-tree';
import ContaItem from '../ContaItem/ContaItem';

const TreeNode = ({ conta, onEdit, onDelete, onSelectConta }) => {
    return (
        <Tree
            content={
                <ContaItem
                    conta={conta}
                    onEdit={onEdit}
                    onDelete={onDelete}
                />
            }
            canHide={true}
        >
            {conta.children && conta.children.map(childConta => (
                <TreeNode
                    key={childConta.indice}
                    conta={childConta}
                    onEdit={onEdit}
                    onDelete={onDelete} 
                />
            ))}
        </Tree>
    );
};

const ContaTreeView = ({ contas, onEdit, onDelete }) => {
    const [contasTree, setContasTree] = useState([]);

    const buildTree = (contas, parentId = null) => {
        const tree = [];
        //console.log('Construindo árvore para parentId:', parentId); // Ver quais são os parentId sendo processados
    
        for (let conta of contas) {
            //console.log('Processando conta:', conta); // Ver as informações de cada conta
    
            if (conta.idPai === parentId || conta.idPai === "") {
                //console.log('Adicionando conta com parentId:', parentId, conta); // Ver quando uma conta é adicionada
    
                const children = buildTree(contas, conta.id);
                if (children.length) {
                    conta.children = children;
                }
                tree.push(conta);
            }
        }
    
        console.log('Árvore construída para parentId', parentId, tree);
        return tree;
    };
    
    useEffect(() => {
        const tree = buildTree(contas);
        console.log("Lista:",contas);
        console.log("Arvore:",tree);
        setContasTree(tree);
    }, [contas]);

    return (
        <div>

            <ul>
                {contasTree.map((conta) => (
                    <TreeNode
                        key={conta.indice}
                        conta={conta}
                        onEdit={onEdit}
                        onDelete={onDelete}
                    />
                ))}
            </ul>
        </div>
    );
};

export default ContaTreeView;
