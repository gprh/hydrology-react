// src/components/ContaItem.js

import React from 'react';
import './ContaItem.css';
import { FaEdit, FaTrash } from 'react-icons/fa'; // Importando ícones

const ContaItem = ({ conta, onDelete, onEdit }) => {
    return (
        <div className="conta-item">
            <div className="conta-details">
                <span>{conta.nome}</span>
            </div>
            <div className="conta-actions">
                <FaEdit className="icon-edit" onClick={() => onEdit(conta)} />
                <FaTrash className="icon-remove" onClick={() => onDelete(conta.id)} />
            </div>
        </div>
    );
}

export default ContaItem;
