import React, { useState, useEffect } from 'react';
import './ContaTable.css'; // Certifique-se de criar um arquivo de estilo CSS apropriado
import { FaEdit, FaTrash } from 'react-icons/fa'; // Importando ícones

import {
    getConta
  } from "../../../api/contaApi";

const ContaTable = ({ contas, onEdit, onDelete }) => {

    // Estado para armazenar os nomes das contas pais
    const [nomesContasPais, setNomesContasPais] = useState({});

    // Função para buscar o nome da conta pai e atualizar o estado
    const fetchNomesContasPais = async (idPai) => {
        try {
            //console.log(`Buscando conta pai com ID: ${idPai}`);
            const conta = await getConta(idPai); 
            //console.log(`Conta recebida:`, conta);           
            setNomesContasPais(prevNomes => {
                const newNomes = { ...prevNomes, [idPai]: conta.nome };
               // console.log(`Novo estado dos nomes das contas pais:`, newNomes);
                return newNomes;
            });
        } catch (error) {
            console.error('Erro ao buscar nome da conta pai:', error);
        }
    };
    // Efeito para buscar os nomes das contas pais quando o componente é montado ou as contas são atualizadas
    useEffect(() => {
        const uniqueParentIds = [...new Set(contas.map(conta => conta.idPai))];
        uniqueParentIds.forEach(idPai => {
            if (idPai && !nomesContasPais[idPai]) {
                fetchNomesContasPais(idPai);
            }
        });
    }, [contas]); // Dependências do efeito

    if (!contas || contas.length === 0) {
        return <p>Nenhuma conta encontrada.</p>;
    }

    const renderCheckbox = (value) => (
        <input type="checkbox" checked={value === 1} readOnly />
    );

    return (
        <table className="conta-table">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Pai</th>
                    <th>Índice</th>
                    <th>É Custo Fixo?</th>
                    <th>É Receita?</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                {contas.map((conta) => (
                    <tr key={conta.id}>
                        <td>{conta.nome}</td>
                        <td>{nomesContasPais[conta.idPai] || 'Carregando...'}</td>                        
                        <td>{conta.indice}</td>
                        <td className="checkbox-cell">
                            {renderCheckbox(conta.isCustoFixo)}
                        </td>
                        <td className="checkbox-cell">
                            {renderCheckbox(conta.isReceita)}
                        </td>
                        <td>
                            <FaEdit className="icon-edit" onClick={() => onEdit(conta)} />
                            <FaTrash className="icon-remove" onClick={() => onDelete(conta.id)} />
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ContaTable;
