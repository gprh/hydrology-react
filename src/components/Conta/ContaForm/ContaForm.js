// src/components/ContaForm.js
import React, { useState, useEffect } from "react";
import Conta from "../../../models/Conta";
import './ContaForm.css'; // Path to your CSS file
import Select from 'react-select';


const ContaForm = ({ onSubmit, contaSelecionada, contas }) => {
  const [contaData, setContaData] = useState(contaSelecionada || new Conta());

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit({
      ...contaData,
      isCustoFixo: contaData.isCustoFixo === true || contaData.isCustoFixo === "1",
      isReceita: contaData.isReceita === true || contaData.isReceita === "1"
    });
    setContaData(new Conta()); // Reset form to new Conta
  };

  const handleSelect = (selectedOption) => {
    setContaData({ ...contaData, idPai: selectedOption ? selectedOption.value : '' })
  };

  return (
    <form onSubmit={handleSubmit} className="conta-form">
      <label>
        Conta:
        <input
          type="text"
          placeholder="Nome da Conta"
          value={contaData.nome}
          onChange={(e) => setContaData({ ...contaData, nome: e.target.value })}
          required
        />
      </label>


      <label>
        ContaPai:
        <Select
          value={contas.find(conta => conta.id === contaData.idPai) ? { value: contaData.idPai, label: contas.find(conta => conta.id === contaData.idPai).nome } : ''}
          onChange={handleSelect}
          options={contas.map(conta => ({ value: conta.id, label: conta.nome }))}
          placeholder="Selecione a conta pai"
        />
      </label>

      <div className="checkbox-container">
        <label>
          <input
            type="checkbox"
            checked={contaData.isCustoFixo}
            onChange={(e) => setContaData({ ...contaData, isCustoFixo: e.target.checked })}
          />
          Custo Fixo
        </label>
      </div>

      <div className="checkbox-container">
        <label>
          <input
            type="checkbox"
            checked={contaData.isReceita}
            onChange={(e) => setContaData({ ...contaData, isReceita: e.target.checked })}
          />
          Receita
        </label>
      </div>

      <button type="submit">{contaSelecionada ? "Atualizar" : "Adicionar"}</button>
    </form>
  );
};

export default ContaForm;