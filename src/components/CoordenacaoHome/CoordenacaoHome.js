import React, { useState } from 'react';
import './CoordenacaoHome.css';
import CadastroProfessores from '../CadastroProfessores';
import AlunoHome from '../../pages/AlunoHome/AlunoHome'
import LancamentoHome from '../../pages/LancamentoHome/LancamentoHome'
import CadastroContas from '../../pages/CadastroContas';

function CoordenacaoHome() {
  const [selectedContent, setSelectedContent] = useState(null);

  return (
    <div className="coordenacao-home">
      <div className="sidebar">
        <h2>Menu</h2>
        <ul>
          
          <li onClick={() => setSelectedContent('lancamento')}>Lancamentos</li>
          <li onClick={() => setSelectedContent('contas')}>Cadastro de Contas</li>
          <li onClick={() => setSelectedContent('professor')}>Cadastro de Professor</li>
          <li onClick={() => setSelectedContent('aluno')}>Cadastro de Aluno</li>
        </ul>
      </div>
      <div className="content">
        {selectedContent === 'contas' && <CadastroContas />}
        {selectedContent === 'lancamento' && <LancamentoHome />}
        {selectedContent === 'professor' && <CadastroProfessores />}
        {selectedContent === 'aluno' && <AlunoHome />}
      </div>
    </div>
  );
}

export default CoordenacaoHome;
