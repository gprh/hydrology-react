import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDHfygMrZUNu9eifNBwJtOa6td2wKSxdW4",
  authDomain: "hydrology-react.firebaseapp.com",
  projectId: "hydrology-react",
  storageBucket: "hydrology-react.appspot.com",
  messagingSenderId: "1015640241273",
  appId: "1:1015640241273:web:272edbc762396e057fd0a8"
};

const app = initializeApp(firebaseConfig);

//Para usar o Firetore
const db = getFirestore(app);

//para fazer autenticacao
const auth = getAuth(app);

export { db, auth };
