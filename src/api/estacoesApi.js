// estacaoApi.js
import { db } from "../firebaseConfig";
import {
  collection,
  query,
  where,
  doc,
  getDocs,
  getDoc,
  addDoc,
  updateDoc,
  deleteDoc
} from "firebase/firestore";

// Função para buscar todas as estações de um projeto específico de um usuário
export const getEstacoes = async (userId, projetoId) => {
  try {
    const estacoesRef = collection(db, "estacoes");
    const q = query(estacoesRef, where("userId", "==", userId), where("projetoId", "==", projetoId));
    const querySnapshot = await getDocs(q);
    const estacoes = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
    return estacoes;
  } catch (error) {
    console.error("Erro ao buscar estações:", error);
    return [];
  }
};

// Função para buscar uma estação específica
export const getEstacao = async (id) => {
  try {
    const docRef = doc(db, "estacoes", id);
    const estacao = await getDoc(docRef);
    return estacao.exists() ? { ...estacao.data(), id: estacao.id } : null;
  } catch (error) {
    console.error("Erro ao buscar estação:", error);
    return null;
  }
};

// Função para criar uma nova estação
export const createEstacao = async (estacao) => {
  try {
    const docRef = await addDoc(collection(db, "estacoes"), estacao);
    console.log("Estação criada com sucesso.");
    return { ...estacao, id: docRef.id };
  } catch (error) {
    console.error("Erro ao criar estação:", error);
    return null;
  }
};

// Função para atualizar uma estação existente
export const updateEstacao = async (id, estacao) => {
  try {
    const estacaoRef = doc(db, "estacoes", id);
    await updateDoc(estacaoRef, estacao);
    console.log("Estação atualizada com sucesso.");
    return { ...estacao, id };
  } catch (error) {
    console.error("Erro ao atualizar estação:", error);
    return null;
  }
};

// Função para deletar uma estação
export const deleteEstacao = async (id) => {
  try {
    const docRef = doc(db, "estacoes", id);
    await deleteDoc(docRef);
    console.log("Estação deletada com sucesso.");
  } catch (error) {
    console.error("Erro ao deletar estação:", error);
  }
};
