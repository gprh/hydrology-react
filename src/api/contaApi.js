import { db, auth } from "../firebaseConfig";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from 'firebase/storage';

import {
  doc, 
  setDoc,
  collection,
  deleteDoc,
  getDocs,
  getDoc,
  addDoc,
  where,
  query
} from "firebase/firestore";
import { createUserWithEmailAndPassword } from "firebase/auth";


export const getConta = async (uid) => {
  try {
    const docRef = await doc(db, "conta", uid); 
    const docSnap = await getDoc(docRef); // Renomeado para docSnap para clareza
    
    if (docSnap.exists()) {
      return docSnap.data(); // Acessa os dados do documento
    } else {
      console.log("Nenhuma conta encontrada com o UID:", uid);
      return null;
    }
  } catch (error) {
    console.error("Erro ao buscar conta:", error);
    return null;
  }
};

export const getContas = async () => {
  try {
    const querySnapshot = await getDocs(collection(db, "conta"));
    const data = querySnapshot.docs.map((doc) => ({
      ...doc.data(),
      id: doc.id,
    }));
    return data;
  } catch (error) {
    console.error("Erro ao buscar contas:", error);
    return [];
  }
};


export const createConta = async (conta) => {
  try {    
    const docRef = await addDoc(collection(db, "conta"), conta);
    console.log("Conta registrado com sucesso.");

    // Inclui o ID do documento recém-criado ao objeto conta
    return { ...conta, id: docRef.id };
  } catch (error) {
    console.error("Erro ao criar conta:", error);
    return null;
  }
};


export const updateConta = async (docId, conta) => {
  try {    
    const contaRef = doc(db, "conta", docId);

    // Atualizar o professor usando setDoc com merge: true para apenas atualizar campos fornecidos
    await setDoc(contaRef, conta, { merge: true });

    console.log("Conta atualizado com sucesso");
    return conta;
  } catch (error) {
    console.error("Erro ao atualizar conta:", error);
    return null;
  }
};

export const deleteConta = async (id) => {
    try {
      // Primeiro, verifica se a conta tem filhos
      const contasRef = collection(db, "conta");
      const q = query(contasRef, where("idPai", "==", id));
      const querySnapshot = await getDocs(q);
  
      if (!querySnapshot.empty) {
        // Se houver documentos na coleção que têm 'idPai' igual ao 'id', a conta tem filhos.
        console.log("Conta não pode ser deletada porque tem contas filhas.");
        return false; // Retorna false indicando que a conta não foi deletada.
      }
  
      // Se não houver filhos, prossegue com a exclusão
      const docRef = doc(db, "conta", id);
      await deleteDoc(docRef);
  
      console.log("Conta deletada com sucesso.");
      return true; // Retorna true indicando que a conta foi deletada.
    } catch (error) {
      console.error("Erro ao deletar conta:", error);
      return false; // Retorna false indicando que a conta não foi deletada.
    }
};



export const handleImageUpload = async (imageFile) => {
  try {
    console.log("Iniciando upload da imagem...");

    const imageRef = ref(getStorage(), `fotoProfessores/${imageFile.name}`);  
    console.log("Referência à imagem criada...");

    const snapshot = await uploadBytesResumable(imageRef, imageFile);
    console.log("Imagem carregada com sucesso...");

    const imageUrl = await getDownloadURL(snapshot.ref);
    console.log("URL de download obtida:", imageUrl);

    return imageUrl;
  } catch (error) {
    console.error("Erro ao fazer upload da imagem:", error);
    return null;
  }
};