// lancamentoAPI.js
import { db } from "../firebaseConfig";
import {
  doc,
  setDoc,
  collection,
  deleteDoc,
  getDocs,
  getDoc,
  addDoc,
  query,
  where
} from "firebase/firestore";

export const getLancamento = async (id) => {
  try {
    const docRef = doc(db, "lancamentos", id);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return { id: docSnap.id, ...docSnap.data() };
    } else {
      console.log("Nenhum lançamento encontrado com o ID:", id);
      return null;
    }
  } catch (error) {
    console.error("Erro ao buscar lançamento:", error);
    return null;
  }
};

export const getLancamentos = async () => {
  try {
    const querySnapshot = await getDocs(collection(db, "lancamentos"));
    return querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
  } catch (error) {
    console.error("Erro ao buscar lançamentos:", error);
    return [];
  }
};

export const createLancamento = async (lancamento) => {
  try {
    const docRef = await addDoc(collection(db, "lancamentos"), lancamento);
    console.log("Lançamento criado com sucesso com ID:", docRef.id);
    return { id: docRef.id, ...lancamento };
  } catch (error) {
    console.error("Erro ao criar lançamento:", error);
    return null;
  }
};

export const updateLancamento = async (id, lancamento) => {
  try {
    const lancamentoRef = doc(db, "lancamentos", id);
    await setDoc(lancamentoRef, lancamento, { merge: true });
    console.log("Lançamento atualizado com sucesso.");
    return { id, ...lancamento };
  } catch (error) {
    console.error("Erro ao atualizar lançamento:", error);
    return null;
  }
};

export const deleteLancamento = async (id) => {
  try {
    await deleteDoc(doc(db, "lancamentos", id));
    console.log("Lançamento deletado com sucesso.");
    return true;
  } catch (error) {
    console.error("Erro ao deletar lançamento:", error);
    return false;
  }
};
