import { db } from "../firebaseConfig";
import {
  doc, 
  setDoc,
  collection,
  deleteDoc,
  getDocs,
  getDoc,
  addDoc,
} from "firebase/firestore";

export const getProjeto = async (id) => {
  try {
    const docRef = doc(db, "projetos", id); 
    const projeto = await getDoc(docRef);    
    return projeto.exists() ? { ...projeto.data(), id: projeto.id } : null;
  } catch (error) {
    console.error("Erro ao buscar projeto:", error);
    return null;
  }
};

export const getProjetos = async () => {
  try {
    const querySnapshot = await getDocs(collection(db, "projetos"));
    const data = querySnapshot.docs.map((doc) => ({
      ...doc.data(),
      id: doc.id,
    }));
    return data;
  } catch (error) {
    console.error("Erro ao buscar projetos:", error);
    return [];
  }
};

export const createProjeto = async (projeto) => {
  try {
    const docRef = await addDoc(collection(db, "projetos"), projeto);
    console.log("Projeto criado com sucesso.");
    return { ...projeto, id: docRef.id };
  } catch (error) {
    console.error("Erro ao criar projeto:", error);
    return null;
  }
};

export const updateProjeto = async (id, projeto) => {
  try {
    const projetoRef = doc(db, "projetos", id);

    // Atualizar o projeto usando setDoc com merge: true para apenas atualizar campos fornecidos
    await setDoc(projetoRef, projeto, { merge: true });

    console.log("Projeto atualizado com sucesso");
    return { ...projeto, id };
  } catch (error) {
    console.error("Erro ao atualizar projeto:", error);
    return null;
  }
};

export const deleteProjeto = async (id) => {
  try {
    // Deletar o projeto do Firestore
    const docRef = doc(db, "projetos", id);
    await deleteDoc(docRef);

    console.log("Projeto deletado com sucesso.");
  } catch (error) {
    console.error("Erro ao deletar projeto:", error);
  }
};
